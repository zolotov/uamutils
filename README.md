# Overview

**uamutils** package provides a Python interface to _Upper Atmosphere Model_ (UAM) binary data format as well as utilities for processing and analysing UAM data.

## Documentation
[This](https://gitlab.com/zolotov/uamutils/-/blob/main/user-docs.ipynb) Jupyter notebook demonstrates essential functionality of the package.

# Caution
This project is a work in progress. Some features are still lacking, while existing features need more rigorous testing. Also, huge refactoring is due — API-breaking changes **will** be made.
# This file is based on https://github.com/pypa/sampleproject

from setuptools import setup, find_packages


short_description = 'Python interface to Upper Atmosphere Model (UAM) binary data format'

with open('README.md') as readme:
    long_description = ''.join(readme.readlines())

setup(
    name='uamutils',

    # versions should comply with PEP 440, https://www.python.org/dev/peps/pep-0440/
    version='0.0.0a3',
    description=short_description,
    long_description=long_description,
    long_description_content_type='text/markdown', # FIXME: markdown bug
    url='https://gitlab.com/zolotov/uamutils',
    license='Apache Software License',
    platform=['any'],
    author='Arthur Orlovskiy, Oleg Zolotov',
    author_email='an.orlovskiy@gmail.com',

    # for a list of valid classifiers, see https://pypi.org/classifiers/
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering :: Physics',
        'License :: OSI Approved :: Apache Software License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3 :: Only',
    ],

    keywords='UAM, Upper Atmosphere Model, MOD4, Earth ionosphere, utils',

    # NOTE: 'cartopy' is optional
    install_requires=['numpy', 'xarray', 'matplotlib'],

    # Automatically finds out all directories (packages) - those must contain a file named __init__.py (can be empty)
    packages=find_packages(),  # include/exclude arguments take * as wildcard, . for any sub-package names
)

'''
# Building a wheel
  $ python setup.py bdist_wheel
or 
  $ pip wheel . --wheel-dir wheels

# Publishing to PyPi
  
test PyPi:
  $ python -m twine upload --repository testpypi <path_to_wheel>

main PyPi:
  $ python -m twine upload <path_to_wheel>
'''